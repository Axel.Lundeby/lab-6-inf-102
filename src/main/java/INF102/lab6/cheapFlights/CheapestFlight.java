package INF102.lab6.cheapFlights;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight<V> implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();

        for (Flight flight : flights) {
            City startCity= flight.start;
            City endCity = flight.destination; 
            int cost = flight.cost; 
        
            graph.addVertex(startCity); 
            graph.addVertex(endCity);

            graph.addEdge(startCity, endCity, cost);
        }

        return graph;
    }
    

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        int[] lowestCost = new int[] { Integer.MAX_VALUE }; // Using array to allow modification within the method
        findPaths(graph, start, destination, nMaxStops + 1, new LinkedList<>(), 0, lowestCost); // nMaxStops + 1 to include the start city
        return lowestCost[0] == Integer.MAX_VALUE ? -1 : lowestCost[0]; // return -1 if no path is found
    }

    private void findPaths(WeightedDirectedGraph<City, Integer> graph, City currentCity, City endCity, int stopsLeft, LinkedList<City> currentPath, int currentWeight, int[] lowestCost) {
        currentPath.add(currentCity);

        if (currentCity.equals(endCity)) {
            if (lowestCost[0] > currentWeight) {
                lowestCost[0] = currentWeight;
            }
        } else if (stopsLeft > 0) {
            // Recursively visit each adjacent city with decremented stops
            for (City neighbor : graph.outNeighbours(currentCity)) {
                if (!currentPath.contains(neighbor)) { // To avoid cycles
                    Integer edgeWeight = graph.getWeight(currentCity, neighbor);
                    findPaths(graph, neighbor, endCity, stopsLeft - 1, currentPath, currentWeight + edgeWeight, lowestCost);
                }
            }
        }

        currentPath.removeLast(); // Backtrack
    }
}

